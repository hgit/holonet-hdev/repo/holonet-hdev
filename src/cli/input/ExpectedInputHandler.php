<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli\input;

use Closure;
use RuntimeException;
use Nette\Schema\Processor;
use holonet\cli\io\InputDevice;
use Nette\Schema\Elements\Type;
use Nette\Schema\Elements\AnyOf;
use holonet\hdev\generator\input\GeneratorInput;
use holonet\hdev\generator\input\ExpectDecorator;

/**
 * Class wrapping around a cli input device and asking for a nette/schema definition from the user.
 */
class ExpectedInputHandler {
	/**
	 * @var array<string, string> $baseData Pre-supplied values to the generator
	 */
	private array $baseData;

	/**
	 * @var ExpectDecorator $expect Wrapper around a nette/schema structure object
	 */
	private ExpectDecorator $expect;

	/**
	 * @var InputDevice $input Reference to the input device to query for answers
	 */
	private InputDevice $input;

	/**
	 * @var Processor $processor Nette schema processor
	 */
	private Processor $processor;

	public function __construct(InputDevice $input, ExpectDecorator $expect, array $baseData) {
		$this->input = $input;
		$this->expect = $expect;
		$this->baseData = $baseData;
		$this->processor = new Processor();
	}

	/**
	 * Ask the user for the required input to run a generator.
	 */
	public function askForExpect(ExpectDecorator $inputDef): GeneratorInput {
		$data = array();
		foreach ($inputDef->getAll() as $name => $item) {
			if (isset($this->baseData[$name])) {
				$data[$name] = $this->baseData[$name];

				continue;
			}

			if ($item instanceof AnyOf) {
				$read = $this->askForAnyOf($item, $inputDef, $name);
			} elseif ($item instanceof Type) {
				$read = $this->askForType($item, $inputDef, $name);
			}

			if (isset($read)) {
				$data[$name] = $read;
			}
		}

		return $this->processor->process($inputDef->struct, $data);
	}

	public function process(): GeneratorInput {
		return $this->askForExpect($this->expect);
	}

	private function askForAnyOf(AnyOf $item, ExpectDecorator $inputDef, string $name) {
		//extract type and required attributes from Expect object
		list($required, $default, $set) = $this->extractPrivateProperties(AnyOf::class, array('required', 'default', 'set'), $item);

		$prompt = $inputDef->getPrompt($name);
		if ($required) {
			return $this->input->getChoice($prompt, array_shift($set));
		}
		if (empty($default)) {
			$prompt .= ' (press ENTER for empty)';

			return $this->input->getChoice($prompt, array_shift($set), '');
		}
	}

	private function askForType(Type $item, ExpectDecorator $inputDef, string $name) {
		list($type, $required, $default) = $this->extractPrivateProperties(Type::class, array('type', 'required', 'default'), $item);
		$type = ltrim($type, '?');

		if (class_exists($type) && is_a($type, GeneratorInput::class, true)) {
			if (!$required) {
				return;
			}

			$item = new $type();

			return $this->askForExpect($item->getSchema());
		}
		$prompt = $inputDef->getPrompt($name);
		if ($required) {
			return $this->input->getNonEmptyInput($prompt);
		}
		if (empty($default)) {
			$prompt .= ' (press ENTER for empty)';

			return $this->input->getInput($prompt);
		}
	}

	private function extractPrivateProperties(string $class, array $properties, object $instance): array {
		$closure = Closure::bind(static function ($instance) use ($properties) {
			$ret = array();
			foreach ($properties as $property) {
				$ret[] = $instance->{$property};
			}

			return $ret;
		}, null, $class);

		if ($closure === false) {
			throw new RuntimeException('Failed to create Closure to extract properties');
		}

		return $closure($instance);
	}
}
