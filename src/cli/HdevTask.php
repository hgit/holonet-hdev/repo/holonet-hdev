<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli;

use holonet\cli\Application;
use holonet\cli\io\InputDevice;
use holonet\cli\io\OutputDevice;
use holonet\holofw\cli\BootstrappedTask;
use holonet\cli\argparse\ArgparseDefinition;

/**
 * Wrap around the standard hdev cli application to provide all the config options just from the application config
 * IF hdev is being used in a holofw application.
 */
class HdevTask extends BootstrappedTask {
	/**
	 * @var HdevApplication $application The hdev application instance we are wrapping for this task
	 */
	private HdevApplication $application;

	public function __construct(InputDevice $in, OutputDevice $out, ArgparseDefinition $argparse) {
		$this->application = new HdevApplication($in, $out, $argparse);

		cli_set_process_title($this->name());

		//make sure php doesn't log fatal errors twice
		ini_set('log_errors', '1');
		ini_set('display_errors', '0');
	}

	/**
	 * {@inheritDoc}
	 */
	public function describe(): string {
		return $this->application->describe();
	}

	/**
	 * {@inheritDoc}
	 */
	public function name(): string {
		return $this->application->name();
	}

	public function run(): void {
		$this->application->config = array(
			'base_dir' => $this->di_context->frameworkStandardPath(''),
			'src_dir' => $this->di_context->appPath(),
			'vendor_name' => $this->di_registry->get('vendorInfo.namespace'),
			'app_name' => $this->di_context->appname,
			'author_name' => $this->di_registry->get('vendorInfo.author.name'),
			'author_email' => $this->di_registry->get('vendorInfo.author.email'),
			'license' => $this->di_registry->get('vendorInfo.license'),
			'part_of' => $this->di_registry->get('vendorInfo.partOf'),
		);

		$this->application->run();
	}

	/**
	 * {@inheritDoc}
	 */
	public function version(): string {
		return $this->application->version();
	}

	/**
	 * {@inheritDoc}
	 */
	protected function configure(): void {
	}

	protected function loadCommands(): void {
	}
}
