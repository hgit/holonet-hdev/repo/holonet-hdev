<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli\gitmirror;

use holonet\cli\Application;

class GitMirrorApplication extends Application {
	/**
	 * {@inheritDoc}
	 */
	public function describe(): string {
		return 'Mirror git repositories between multiple origins.';
	}

	/**
	 * {@inheritDoc}
	 */
	public function name(): string {
		return 'git-mirror';
	}

	/**
	 * {@inheritDoc}
	 */
	public function version(): string {
		return '1.0.0';
	}

	/**
	 * {@inheritDoc}
	 */
	protected function configure(): void {
		$this->argumentDefinition->addOption('t|target', 'target', 'Target base directory (defaults to cwd())')->default(getcwd());
		$this->argumentDefinition->addOption('g|gitbin', 'git', 'Path to the git binary used on the system')->default('git');
	}

	/**
	 * {@inheritDoc}
	 */
	protected function loadCommands(): void {
		$this->addCommand(new commands\ListCommand());
		$this->addCommand(new commands\AddCommand());
		$this->addCommand(new commands\MirrorCommand());
	}
}
