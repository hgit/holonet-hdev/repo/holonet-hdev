<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli\gitmirror\commands;

use holonet\cli\argparse\Argument;

class AddCommand extends GitMirrorBaseCommand {
	/**
	 * {@inheritDoc}
	 */
	public function configure(): void {
		$this->argumentDefinition->addOption('s|source', 'source', 'Source repository url');
		$this->argumentDefinition->addOption('m|mirrors', 'mirrors', 'Target repository url', Argument::NARGS_ARRAY)->optional(true);
	}

	/**
	 * {@inheritDoc}
	 */
	public function describe(): string {
		return 'Add a new repository mirror from SOURCE to MIRROR';
	}

	/**
	 * {@inheritDoc}
	 */
	public function execute(): void {
		$existingMirrors = $this->collectGitMirrorRepos()[$this->input->getArg('source')] ?? array();

		$targetDirectoryName = $this->safefifyUrls($this->input->getArg('source'));
		$targetDirectory = "{$this->input->getArg('target')}/{$targetDirectoryName}";
		if (file_exists($targetDirectory)) {
			$this->output->writeOutLn("Mirror repository '{$targetDirectory}' already exists, adding new mirror remotes...");
		} else {
			$this->output->writeOutLn("Cloning {$this->input->getArg('source')} to {$targetDirectory}");
			$this->output->writeOut($this->execGit("clone {$this->input->getArg('source')} {$targetDirectoryName}", $this->input->getArg('target')));
		}

		foreach ($this->input->getArg('mirrors') as $mirror) {
			if (in_array($mirror, $existingMirrors)) {
				$this->output->writeOut("\tMirror '{$mirror}': already exists in repo");

				continue;
			}

			$mirrorName = $this->safefifyUrls($mirror);
			$this->output->writeOut($this->execGit("remote add {$mirrorName} {$mirror}", $targetDirectory));
			$this->output->writeOut("\tMirror '{$mirror}': added to repo");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function name(): string {
		return 'add';
	}
}
