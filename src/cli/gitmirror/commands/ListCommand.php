<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli\gitmirror\commands;

class ListCommand extends GitMirrorBaseCommand {
	/**
	 * {@inheritDoc}
	 */
	public function configure(): void {
	}

	/**
	 * {@inheritDoc}
	 */
	public function describe(): string {
		return 'List all currently existing mirror repositories inside the target directory';
	}

	/**
	 * {@inheritDoc}
	 */
	public function execute(): void {
		$repos = $this->collectGitMirrorRepos();
		foreach ($repos as $source => $mirrors) {
			$this->output->writeOutLn("Mirroring from '{$source}' to:");
			foreach ($mirrors as $mirror) {
				$this->output->writeOutLn("\t{$mirror}");
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function name(): string {
		return 'list';
	}
}
