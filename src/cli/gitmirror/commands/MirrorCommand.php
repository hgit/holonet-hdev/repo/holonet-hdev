<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli\gitmirror\commands;

use InvalidArgumentException;

class MirrorCommand extends GitMirrorBaseCommand {
	/**
	 * {@inheritDoc}
	 */
	public function configure(): void {
		$this->argumentDefinition->addArgument('repo', 'which repo to mirror (defaults to all)')->optional(true);
	}

	/**
	 * {@inheritDoc}
	 */
	public function describe(): string {
		return 'Mirror all changes from the origin remote to all the mirror remotes';
	}

	/**
	 * {@inheritDoc}
	 */
	public function execute(): void {
		$existingMirrors = $this->collectGitMirrorRepos();

		if (($repoToMirror = $this->input->getArg('repo')) !== null) {
			if (!isset($existingMirrors[$repoToMirror])) {
				throw new InvalidArgumentException("Unknown repository to mirror: '{$repoToMirror}'");
			}

			$this->mirrorRepo($repoToMirror, $existingMirrors[$repoToMirror]);

			return;
		}

		foreach ($existingMirrors as $source => $mirrors) {
			$this->mirrorRepo($source, $mirrors);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function name(): string {
		return 'mirror';
	}

	private function mirrorRepo(string $source, array $mirrors): void {
		$targetDirectoryName = $this->safefifyUrls($source);
		$this->output->writeOutLn("Pulling changes into '{$targetDirectoryName}'");
		$targetDirectory = "{$this->input->getArg('target')}/{$targetDirectoryName}";
		$pullResult = $this->execGit('pull origin', $targetDirectory);
		$this->output->writeOutLn($pullResult);

		foreach ($mirrors as $mirrorToPush) {
			$mirrorName = $this->safefifyUrls($mirrorToPush);
			$this->output->writeOutLn("\nPush changes to '{$mirrorToPush}'");
			$this->output->writeOut($this->execGit("push {$mirrorName}", $targetDirectory));
		}
	}
}
