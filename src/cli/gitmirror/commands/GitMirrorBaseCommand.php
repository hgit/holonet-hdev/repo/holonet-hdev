<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli\gitmirror\commands;

use SplFileInfo;
use RuntimeException;
use DirectoryIterator;
use holonet\cli\Command;

abstract class GitMirrorBaseCommand extends Command {
	protected function collectGitMirrorRepos(): array {
		$gitMirrors = array();
		$this->output->writeOutLn("Checking for git mirror repositories in '{$this->input->getArg('target')}'");
		$iter = new DirectoryIterator($this->input->getArg('target'));
		foreach ($iter as $file) {
			if ($file->isDir() && !$file->isDot()) {
				$this->output->writeOutLn("Checking '{$file}'");
				$this->tryToDiscoverGitRepo($file, $gitMirrors);
			}
		}
		$this->output->writeOutLn('');

		return $gitMirrors;
	}

	protected function execGit(string $cmd, ?string $path = null, bool $ignoreFailure = false): string {
		$out = array();
		$returnVal = 0;
		$cmd = sprintf('"%s" %s %s 2>&1',
			$this->input->getArg('git'),
			($path === null ? '' : '-C '.escapeshellarg($path)),
			$cmd
		);

		exec($cmd, $out, $returnVal);

		$ret = implode("\n", $out);

		if (mb_strpos($ret, 'fatal:') === 0) {
			throw new RuntimeException("Error running git command '{$cmd}'({$returnVal}):\n {$ret}", $returnVal);
		}

		if ($returnVal !== 0) {
			if ($ignoreFailure) {
				return trim($ret);
			}

			throw new RuntimeException("Error running git command '{$cmd}'({$returnVal}):\n {$ret}", $returnVal);
		}

		return trim($ret);
	}

	protected function safefifyUrls(string $url): string {
		return str_replace(array(':', '/'), '-', $url);
	}

	private function tryToDiscoverGitRepo(SplFileInfo $file, array &$repos): void {
		if (!file_exists("{$file}/.git") && is_dir("{$file}/.git")) {
			$this->output->writeOutLn("'{$file}': not a git repository");
		}

		$origin = null;
		$mirrors = array();

		$remotes = explode("\n", $this->execGit('remote -v', $file->getRealPath()));
		foreach ($remotes as $remote) {
			$remote = preg_split('/ |\t/', $remote);
			list($name, $url, $type) = $remote;
			if ($name === 'origin') {
				if ($type === '(fetch)') {
					$origin = $url;
				}
			} elseif ($type === '(push)') {
				$mirrors[] = $url;
			}
		}
		if (isset($origin)) {
			$repos[$origin] = $mirrors;
		} elseif (!empty($mirrors)) {
			throw new RuntimeException("Directory '{$file}' has mirror remotes, but no source 'origin' remote");
		}
	}
}
