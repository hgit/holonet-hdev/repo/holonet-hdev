<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli;

use holonet\cli\Command;
use holonet\cli\Application;
use holonet\hdev\generator\HdevGenerator;
use holonet\hdev\cli\commands\InitCommand;
use holonet\common\error\BadEnvironmentException;

class HdevApplication extends Application {
	/**
	 * @var array<string, mixed> $config Array with the hdev config (vendor information)
	 */
	public array $config;

	/**
	 * {@inheritDoc}
	 */
	public function describe(): string {
		return 'Provide command line utility for a developer working with the holonet framework.';
	}

	/**
	 * {@inheritDoc}
	 */
	public function name(): string {
		return 'hdev';
	}

	public function runCommand(Command $cmd2run): void {
		/**
		 * @psalm-suppress TypeDoesNotContainType
		 * @todo somehow psalm doesn't pick up on typed properties checking with isset()
		 */
		if (!$cmd2run instanceof InitCommand && !isset($this->config)) {
			$this->config = $this->loadConfig();
			$this->config['base_dir'] = getcwd();
		}
		parent::runCommand($cmd2run);
	}

	/**
	 * {@inheritDoc}
	 */
	public function version(): string {
		return '2.0.0';
	}

	/**
	 * {@inheritDoc}
	 */
	protected function configure(): void {
	}

	/**
	 * {@inheritDoc}
	 */
	protected function loadCommands(): void {
		$this->addCommand(new commands\GenerateCommand());
		$this->addCommand(new commands\InitCommand());
	}

	/**
	 * {@inheritDoc}
	 * Overwritten so we can add a list of available generators to the output.
	 */
	protected function usageText(bool $includeDescription = false): string {
		$usageText = parent::usageText($includeDescription)."Available code generators:\n";

		foreach (HdevGenerator::GENERATORS as $name => $generator) {
			$usageText .= "  {$name}";
		}

		return "{$usageText}\n";
	}

	private function loadConfig(): array {
		$configJson = getcwd().'/hdev.json';
		if (!file_exists($configJson)) {
			$homepath = $_SERVER['HOME'] ?? "{$_SERVER['HOMEDRIVE']}{$_SERVER['HOMEPATH']}";
			$configJson = "{$homepath}/hdev.json";
		}

		if (file_exists($configJson) && is_readable($configJson)) {
			$config = file_get_contents($configJson);

			return json_decode($config, true, 512, \JSON_THROW_ON_ERROR);
		}

		throw new BadEnvironmentException('Neither local or global hdev.json config file found. Run hdev init first');
	}
}
