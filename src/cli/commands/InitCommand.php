<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli\commands;

use holonet\cli\Command;
use Codedungeon\PHPCliColors\Color;

class InitCommand extends Command {
	/**
	 * {@inheritDoc}
	 */
	public function configure(): void {
		$this->argumentDefinition->addFlag('g|global', 'global', 'Run hdev in global (machine-wide) mode');
	}

	/**
	 * {@inheritDoc}
	 */
	public function describe(): string {
		return 'Initialise the hdev environment';
	}

	/**
	 * {@inheritDoc}
	 */
	public function execute(): void {
		$config = $this->askForConfig();

		if ($this->input->getArg('global')) {
			$homepath = $_SERVER['HOME'] ?? "{$_SERVER['HOMEDRIVE']}{$_SERVER['HOMEPATH']}";
			$configJson = "{$homepath}/hdev.json";
		} else {
			$configJson = getcwd().'/hdev.json';
		}

		file_put_contents($configJson, json_encode($config, \JSON_PRETTY_PRINT));
		$this->output->writeOutLn("Successfully generated '{$configJson}'", Color::GREEN);
	}

	/**
	 * {@inheritDoc}
	 */
	public function name(): string {
		return 'init';
	}

	private function askForConfig(): array {
		do {
			$config = array();

			$config['src_dir'] = $this->input->getInput('Relative src/ directory (default ./src)');
			if (empty($config['src_dir'])) {
				$config['src_dir'] = 'src';
			}

			$config['vendor_name'] = $this->input->getNonEmptyInput('Vendor name (namespace prefix)');
			$config['app_name'] = $this->input->getNonEmptyInput('Application name (base namespace)');
			$config['generate_headers'] = $this->input->getYesOrNo('Generate php file headers with license and author info?');
			$config['author_name'] = $this->input->getNonEmptyInput('Author name');

			$partOf = $this->input->getInput('File header package partOf (press ENTER for none)');
			if (!empty($partOf)) {
				$config['part_of'] = $partOf;
			}
			$email = $this->input->getInput('Author email (press ENTER for none)');
			if (!empty($email)) {
				$config['author_email'] = $email;
			}

			$config['license'] = $this->input->getNonEmptyInput('Code License');

			$this->output->writeOutLn('Generated config:');
			$this->output->writeOutLn(json_encode($config, \JSON_PRETTY_PRINT));
			$accept = $this->input->getYesOrNo('OK?');
		} while (!$accept);

		return $config;
	}
}
