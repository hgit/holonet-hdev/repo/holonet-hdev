<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\cli\commands;

use holonet\cli\Command;
use InvalidArgumentException;
use Codedungeon\PHPCliColors\Color;
use holonet\hdev\cli\HdevApplication;
use holonet\hdev\generator\BaseGenerator;
use holonet\hdev\generator\HdevGenerator;
use holonet\cli\error\InvalidUsageException;
use holonet\hdev\cli\input\ExpectedInputHandler;

/**
 * @property HdevApplication $cliapp
 */
class GenerateCommand extends Command {
	/**
	 * @var HdevGenerator $generator Instance of the generator
	 */
	private HdevGenerator $generator;

	/**
	 * {@inheritDoc}
	 */
	public function configure(): void {
		$this->argumentDefinition->addArgument('generator', 'Which generator to call')->optional(true);
	}

	/**
	 * {@inheritDoc}
	 */
	public function describe(): string {
		return 'Scaffold classes and entities in a holofw application';
	}

	/**
	 * {@inheritDoc}
	 */
	public function execute(): void {
		$this->generator = new HdevGenerator($this->cliapp->config);

		$calledGenerator = $this->input->getArg('generator') ?? $this->input->getNonEmptyInput('Choose a generator to call');

		try {
			$this->runGenerator($this->generator->getGenerator($calledGenerator));
		} catch (InvalidArgumentException $e) {
			throw new InvalidUsageException($e->getMessage(), (int)$e->getCode(), $e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function name(): string {
		return 'generate';
	}

	private function runGenerator(BaseGenerator $generator): void {
		$this->output->writeOutLn("\nRunning generator ".get_class($generator)."\n", Color::GREEN);
		$inputDef = $generator->getInputDef()->getSchema();

		$expectInput = new ExpectedInputHandler($this->input, $inputDef, $this->generator->getGlobalData());

		$file = $this->generator->run($generator, $expectInput->process());
		$this->output->writeOutLn("\nGenerated new php file '{$file}'", Color::GREEN);
	}
}
