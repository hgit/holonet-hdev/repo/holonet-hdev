<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\testing;

use RuntimeException;
use holonet\holofw\Context;
use holonet\holofw\FWApplication;
use holonet\activerecord\Database;
use holonet\common\FilesystemUtils;
use holonet\activerecord\ModelRepository;
use holonet\common\collection\ConfigRegistry;
use holonet\common\error\BadEnvironmentException;
use holonet\holofw\auth\handler\FlatfileAuthHandler;

/**
 * FwAppTestContext bootstrapped holofw application context for testing.
 */
class FwAppTestContext {
	public array $testEnv = array(
		'APP_ENV' => 'testing',
		'DEV_MODE' => '0'
	);

	protected FWApplication $app;

	public function __construct() {
		try {
			foreach ($this->testEnv as $key => $item) {
				$_ENV[$key] = $item;
			}
			$appclass = FWApplication::discoverApplicationClass();
			$this->app = new $appclass();

			// clean up any residue testing var files from a possible faulty or crashed run
			FilesystemUtils::rrmdir($this->app->context->varPath());
			$this->app->context->initVarDirs();

			// override the database configuration that was loaded from ENV
			$this->app->registry->set('database.driver', 'sqlite');

			if (is_a($this->app->registry->get('auth.handler', ''), FlatfileAuthHandler::class, true)) {
				$this->app->registry->set('auth.local_file', $this->app->context->varPath('localauth.php'));
				$this->testEnv['LOCAL_AUTH_FILE_PATH'] = $this->app->context->varPath('localauth.php');
			}

			$this->app->bootstrap();

			// wrap the default sqlite connector into a TestConnector instance
			$this->db()->connector = new TestConnector($this->db()->connector);

			$this->setUpDatabase();
		} catch (RuntimeException $e) {
			throw new RuntimeException("Failed to boot holonet framework application test context: {$e->getMessage()}", (int)$e->getCode(), $e);
		}
	}

	public function __destruct() {
		$path = $this->app->context->varPath();
		unset($this->app);
		// this is necessary to release the db file lock that the application instance had
		gc_collect_cycles();
		// clean up test var dir
		FilesystemUtils::rrmdir($path, true);
	}

	public function conn(): TestConnector {
		return $this->app->container->get('database')->connector;
	}

	public function context(): Context {
		return $this->app->context;
	}

	public function db(): Database {
		return $this->app->container->get('database');
	}

	public function dbfixture(string $table, array $data): void {
		foreach ($data as $row) {
			$sql = sprintf('INSERT INTO "%s" ("%s") VALUES (\'%s\')',
				$table,
				implode('", "', array_keys($row)),
				implode("', '", array_values($row))
			);

			$this->conn()->queryChange($sql);
		}
	}

	public function dependency(string $id) {
		return $this->app->container->get($id);
	}

	public function registry(): ConfigRegistry {
		return $this->app->registry;
	}

	public function repo(): ModelRepository {
		return $this->app->container->get('database')->repository;
	}

	public function rootdir(): string {
		return $this->app->context->frameworkStandardPath('');
	}

	private function setUpDatabase(): void {
		$database = $this->db();
		$connector = $this->conn();

		$schemaFile = $this->app->context->appPath('db/schema.php');
		if (!file_exists($schemaFile)) {
			throw new BadEnvironmentException("Could not find database schema file to load '{$schemaFile}'");
		}
		require $schemaFile;

		$schemaFile = $this->app->context->appPath('db/seeds.php');
		if (!file_exists($schemaFile)) {
			throw new BadEnvironmentException("Could not find database seeds file to load '{$schemaFile}'");
		}

		require $schemaFile;
	}
}
