<?php
/**
 * This file is part of the pong tourney planner app
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\testing;

use LogicException;
use RuntimeException;
use holonet\common\Noun;
use Behat\Gherkin\Node\TableNode;
use holonet\activerecord\Database;
use Symfony\Component\Process\Process;
use holonet\activerecord\ModelRegistry;
use holonet\holofw\auth\flow\PromptAuthFlow;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Mink\Exception\ExpectationException;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use holonet\common\error\BadEnvironmentException;
use Behat\Behat\Tester\Exception\PendingException;

class FwBootstrappedMinkContext extends BaseFeatureContext {
	protected array $basicauth = array();

	protected FwAppTestContext $testContext;

	protected array $testUsers = array();

	/**
	 * @var Process $serverProcess Symfony process of a development server for running the tests against
	 */
	private Process $serverProcess;

	/**
	 * @AfterScenario
	 */
	public function after(AfterScenarioScope $scope): void {
		$this->serverProcess->stop();
		unset($this->serverProcess);
		$this->getSession()->reset();
		unset($this->testContext);
	}

	/**
	 * @BeforeScenario
	 */
	public function before(BeforeScenarioScope $scope): void {
		$this->testContext = new FwAppTestContext();
		$this->startTestServer();
	}

	/**
	 * @Given /^I am authenticated as "([^"]*)"$/
	 * @Given /^I am authenticated as "([^"]*)" with password "([^"]*)"$/
	 */
	public function iAmAuthenticatedAs(string $username, ?string $password = null): void {
		$password ??= $this->testUsers[$username] ?? $username;

		if (is_a($this->testContext->registry()->get('auth.flow', ''), PromptAuthFlow::class, true)) {
			$this->basicauth = array('username' => $username, 'password' => $password);

			$session = $this->getSession();
			if (!$session->isStarted()) {
				$session->start();
			}
			$session->setBasicAuth($username, $password);
		} else {
			throw new PendingException('Define application specific test authentication method iAmAuthenticatedAs()');
		}
		$this->visitPath('/');
		$this->assertResponseStatus(200);
	}

	/**
	 * @Given /^I should not see any form error$/
	 */
	public function iShouldNotSeeAnyFormError(): void {
		$errorFields = $this->getSession()->getPage()->findAll('css', 'div.invalid-feedback:not(:empty)');

		$messages = array();
		foreach ($errorFields as $element) {
			$messages[] = $element->getText();
		}
		$message = sprintf(
			'No form errors should be on the page, but the following are: %s',
			json_encode($messages)
		);

		if (!empty($messages)) {
			throw new ExpectationException($message, $this->getSession()->getDriver());
		}
	}

	/**
	 * @Given /^I should see a form error "([^"]*)"$/
	 */
	public function iShouldSeeAFormError(string $errorMessage): void {
		$this->assertSession()->elementExists('css', sprintf('div.invalid-feedback:contains("%s")', $errorMessage));
	}

	public function locatePath($path): string {
		$url = parent::locatePath($path);
		if (empty($this->basicauth)) {
			return $url;
		}

		$parsed = parse_url($url);
		$replace = "{$parsed['scheme']}://{$parsed['host']}";
		$hostWithCredentials = "{$this->basicauth['username']}:{$this->basicauth['password']}@{$parsed['host']}";

		return str_replace($replace, "{$parsed['scheme']}://{$hostWithCredentials}", $url);
	}

	/**
	 * @Given /^the following "([^"]*)" database entries exist:$/
	 */
	public function theFollowingDatabaseEntriesExist(string $modelName, TableNode $table): void {
		$this->testContext->dbfixture($modelName, $table->getHash());
	}

	/**
	 * @Given /^the following "([^"]*)" exist:$/
	 */
	public function theFollowingEntriesExist(string $modelName, TableNode $table): void {
		$modelClass = ModelRegistry::byName(Noun::singularise($modelName));

		foreach ($table as $row) {
			$this->testContext->repo()->mustcreate($modelClass, $row);
		}
	}

	/**
	 * @Given /^the following local auth user entries exist:$/
	 */
	public function theFollowingLocalAuthUserEntriesExist(TableNode $table): void {
		$newUsers = array();
		foreach ($table->getHash() as $userRow) {
			if (!isset($userRow['username'])) {
				throw new LogicException('Cannot create local auth user without the "username" field');
			}

			$userRow['password'] ??= $userRow['username'];
			if (isset($userRow['permissions'])) {
				$userRow['permissions'] = explode(',', $userRow['permissions']);
			} else {
				$userRow['permissions'] = array();
			}

			$newUsers[$userRow['username']] = $userRow;
			$this->testUsers[$userRow['username']] = $userRow['password'];
		}

		$configfile = $this->testContext->context()->varPath('localauth.php');
		if (file_exists($configfile)) {
			$ret = require $configfile;
			//either the user sets a variable called "users" or returns an array
			if (!isset($users) && ($users = $ret) === 1) {
				throw new BadEnvironmentException("Could not parse php users list '{$configfile}'; File must either return an array or define the variable \$users");
			}

			$newUsers = array_merge($users, $newUsers);
		}

		if (!file_put_contents($configfile, "<?php\nreturn ".var_export($newUsers, true).';')) {
			throw new RuntimeException("Could not write to testing localauth.php '{$configfile}'");
		}
	}

	private function startTestServer(): void {
		$behatConfig = "{$this->testContext->rootdir()}behat.yml";
		if (!file_exists($behatConfig) && is_readable($behatConfig)) {
			throw new RuntimeException("Could not read '{$behatConfig}'; Can only be used with a behat enabled application");
		}

		$behatConfigContent = file_get_contents($behatConfig);
		if (!preg_match('/base_url: ".*?(\d+)"/', $behatConfigContent, $matches)) {
			throw new RuntimeException("Could not read '{$behatConfig}'; Unable to parse behat base url port");
		}

		$port = (int)($matches[1]);

		$this->serverProcess = new Process(
			array('vendor/bin/server', '--port', $port, '--host', '127.0.0.1'),
			$this->testContext->rootdir(),
			$this->testContext->testEnv,
			null, null
		);

		/**
		 * @psalm-suppress RedundantCondition
		 * Will be removed in 5.0.0, here and required now
		 */
		if (method_exists($this->serverProcess, 'inheritEnvironmentVariables')) {
			$this->serverProcess->inheritEnvironmentVariables(true);
		}
		$this->serverProcess->start();

		usleep(25000);
		if (!$this->serverProcess->isRunning()) {
			throw new RuntimeException("Failed to run holofw development server in background process: {$this->serverProcess->getErrorOutput()}");
		}
	}
}
