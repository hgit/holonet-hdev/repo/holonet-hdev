<?php
/**
 * This file is part of the pong tourney planner app
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\testing;

use Exception;
use Behat\Mink\Element\Element;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Mink\Exception\ExpectationException;

/**
 * Contains all general purpose utility feature steps that I think should even be
 * in the base class.
 */
class BaseFeatureContext extends MinkContext {
	/**
	 * @When /^I click "([^"]*)" in the "([^"]*)" table row "([^"]*)"$/
	 * @When /^I follow "([^"]*)" in the "([^"]*)" table row "([^"]*)"$/
	 */
	public function iClickInTheTableRow(string $btn, string $table, string $text): void {
		$row = $this->findRowByText($text, $this->findTableByCaption($table));

		$this->assertSession()->elementExists('named', array('link_or_button', $btn), $row);
		$link = $row->find('named', array('link_or_button', $btn));
		$link->click();
	}

	/**
	 * @Then /^I should see a "([^"]*)" table with (\d+) elements$/
	 * @Then /^I should see a "([^"]*)" table with 1 element$/
	 */
	public function iShouldSeeATableWithElements(string $tableCaption, int $rowCount = 1): void {
		$this->assertSession()->elementsCount('css', 'tbody > tr', $rowCount, $this->findTableByCaption($tableCaption));
	}

	/**
	 * @Then /^the current url should match the regex "([^"]+)"$/
	 */
	public function theCurrentUrlShouldMatchTheRegex(string $regexPath): void {
		$actual = $this->getSession()->getCurrentUrl();
		$expected = str_replace('/', '\/', $this->locatePath($regexPath));

		if (!preg_match("/^{$expected}$/", $actual)) {
			throw new ExpectationException(sprintf('Current page is "%s", but something matching regex "%s" expected.', $actual, $expected), $this->getSession()->getDriver());
		}
	}

	/**
	 * @Given /^wait for ajax requests to be complete$/
	 */
	public function waitForAjaxRequestsToBeComplete(): void {
		$this->getSession()->wait(2000, '(0 === jQuery.active)');
	}

	protected function findRowByText(string $rowText, ?Element $table = null) {
		//if the table was not given, search on the entire page
		if ($table === null) {
			$table = $this->getSession()->getPage();
		}

		$this->assertSession()->elementExists('css', sprintf('table tr:contains("%s")', $rowText), $table);
		$rows = $table->findAll('css', sprintf('table tr:contains("%s")', $rowText));
		if (count($rows) !== 1) {
			$found = count($rows);

			throw new Exception("String '{$rowText}' could not be used to find a unique table row (found {$found}).");
		}

		return array_shift($rows);
	}

	protected function findTableByCaption(string $caption): Element {
		$page = $this->getSession()->getPage();
		$this->assertSession()->elementExists('named', array('table', $caption), $page);

		$tables = $page->findAll('named', array('table', $caption));
		if (count($tables) !== 1) {
			$found = count($tables);

			throw new Exception("Caption '{$caption}' could not be used to find a unique table (found {$found}).");
		}

		return array_shift($tables);
	}
}
