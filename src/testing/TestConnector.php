<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\testing;

use LogicException;
use holonet\dbconnect\Connector;

/**
 * TestConnector wraps around a normal Connector object and offers utility
 * functions to check the number of queries an action is taking.
 */
class TestConnector extends Connector {
	public array $log = array();

	public int $queryCounter = 0;

	private Connector $connector;

	public function __construct(Connector $connector) {
		$this->connector = $connector;
	}

	public static function canBeUsed(): bool {
		return true;
	}

	public function commit(): bool {
		return $this->connector->commit();
	}

	public function connect(): void {
		$this->connector->connect();
	}

	public function exec(string $query, ?array $data = null): bool {
		$this->queryCounter++;
		$this->log[] = "exec:        {$query}";

		return $this->connector->exec($query, $data);
	}

	public function lastInsertId(string $table): int {
		return $this->connector->lastInsertId($table);
	}

	public static function providesDbms(): array {
		return array('sqlite');
	}

	public function queryAll(string $query, array $data = array()): array {
		$this->queryCounter++;
		$this->log[] = "queryAll:    {$query}";

		return $this->connector->queryAll($query, $data);
	}

	public function queryChange(string $query, array $data = array()): int {
		$this->queryCounter++;
		$this->log[] = "queryChange: {$query}";

		return $this->connector->queryChange($query, $data);
	}

	public function queryColumn(string $query, array $data = array()) {
		$this->queryCounter++;
		$this->log[] = "queryColumn: {$query}";

		return $this->connector->queryColumn($query, $data);
	}

	public function queryRow(string $query, array $data = array()): ?array {
		$this->queryCounter++;
		$this->log[] = "queryRow:    {$query}";

		return $this->connector->queryRow($query, $data);
	}

	public function resetState(): void {
		$this->queryCounter = 0;
		$this->log = array();
	}

	public function rollback(): bool {
		return $this->connector->rollback();
	}

	public function transaction(): bool {
		return $this->connector->transaction();
	}

	protected function beginTransaction(): bool {
		throw new LogicException('How did we get here');
	}

	protected function commitTransaction(): bool {
		throw new LogicException('How did we get here');
	}

	protected function rollbackTransaction(): bool {
		throw new LogicException('How did we get here');
	}
}
