<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\testing;

use PHPUnit\Framework\TestCase;
use holonet\activerecord\ModelRepository;

abstract class FwBootstrappedUnitTest extends TestCase {
	protected FwAppTestContext $testContext;

	protected function setUp(): void {
		$this->testContext = new FwAppTestContext();
	}

	protected function tearDown(): void {
		unset($this->testContext);
	}

	protected function assertQueryCounterIs(int $count): void {
		$this->assertSame($count, $this->conn()->queryCounter,
			"Failed asserting number of executed queries was {$count}. Executed queries: \n\t".implode("\n\t", $this->conn()->log)
		);
		$this->conn()->resetState();
	}

	protected function conn(): TestConnector {
		return $this->testContext->conn();
	}

	protected function fixture(string $table, array $data): void {
		$this->testContext->dbfixture($table, $data);
	}

	protected function repo(): ModelRepository {
		return $this->testContext->repo();
	}
}
