<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\input;

use Nette\Schema\Expect;

abstract class GeneratorInput {
	/**
	 * @var array<string, string> PROMPTS
	 */
	public const PROMPTS = array();

	public string $appname;

	abstract public function getFilePath();

	public function getSchema(): ExpectDecorator {
		return new ExpectDecorator(Expect::from($this), static::PROMPTS);
	}
}
