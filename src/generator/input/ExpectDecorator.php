<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\input;

use Closure;
use RuntimeException;
use Nette\Schema\Schema;
use InvalidArgumentException;
use Nette\Schema\Elements\Structure;

class ExpectDecorator {
	/**
	 * The contained nette expect structure being decorated.
	 */
	public Structure $struct;

	/**
	 * @var Schema[] $items
	 */
	private array $items;

	/**
	 * @var array<string, string> $prompts Key value for custom prompt messages
	 */
	private array $prompts;

	public function __construct(Structure $struct, array $prompts = array()) {
		$this->struct = $struct;
		$this->prompts = $prompts;

		//extract items array from structure
		$closure = Closure::bind(static fn (Structure $class) => $class->items, null, Structure::class);

		if ($closure === false) {
			throw new RuntimeException('Failed to create Closure to extract properties');
		}

		$this->items = $closure($struct);
	}

	public function get(string $item): Schema {
		if (!isset($this->items[$item])) {
			throw new InvalidArgumentException("Structure does not expect a property called '{$item}'");
		}

		return $this->items[$item];
	}

	/**
	 * @return array<string, Schema> the contained items
	 */
	public function getAll(): array {
		return $this->items;
	}

	public function getPrompt(string $item) {
		$this->get($item);

		return $this->prompts[$item] ?? "{$item}";
	}

	public function remove(string $item): void {
		$this->get($item);
		unset($this->items[$item]);
	}

	public function set(string $key, Schema $item): void {
		$this->items[$key] = $item;
	}
}
