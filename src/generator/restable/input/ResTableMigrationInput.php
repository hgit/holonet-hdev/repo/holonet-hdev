<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\restable\input;

use holonet\activerecord\Utils;
use holonet\hdev\generator\table\input\TableMigrationInput;

/**
 * @psalm-suppress MissingConstructor
 */
class ResTableMigrationInput extends TableMigrationInput {
	public const PROMPTS = array(
		'tableOne' => 'name of the first table',
		'tableTwo' => 'name of the second table',
	);

	public string $table = '_generated_';

	public string $tableOne;

	public string $tableTwo;

	public function getTablename(): string {
		return Utils::resolutionTable($this->tableOne, $this->tableTwo);
	}
}
