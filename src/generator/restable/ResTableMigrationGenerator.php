<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\restable;

use InvalidArgumentException;
use Nette\PhpGenerator\ClassType;
use holonet\hdev\generator\input\GeneratorInput;
use holonet\hdev\generator\migration\MigrationFileGenerator;
use holonet\hdev\generator\migration\input\MigrationFileInput;
use holonet\hdev\generator\restable\input\ResTableMigrationInput;

class ResTableMigrationGenerator extends MigrationFileGenerator {
	public function getInputDef(): GeneratorInput {
		return new ResTableMigrationInput();
	}

	protected function addMigrationMethods(MigrationFileInput $generatorInput, ClassType $class): void {
		if (!$generatorInput instanceof ResTableMigrationInput) {
			throw new InvalidArgumentException(sprintf('Generator %s expects generator input of type %s', static::class, ResTableMigrationInput::class));
		}

		$method = $class->addMethod('up');
		$method->setReturnType('void');
		$method->setBody("\$this->schema->createResolutionTable('{$generatorInput->tableOne}', '{$generatorInput->tableTwo}');");

		$method = $class->addMethod('down');
		$method->setReturnType('void');
		$method->setBody("\$this->schema->dropTable('{$generatorInput->getTablename()}');");
	}
}
