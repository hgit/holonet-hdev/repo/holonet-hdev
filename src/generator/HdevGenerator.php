<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator;

use RuntimeException;
use InvalidArgumentException;
use Nette\PhpGenerator\PhpFile;
use holonet\hdev\generator\input\GeneratorInput;
use holonet\common\error\BadEnvironmentException;
use holonet\hdev\generator\classfile\ClassFileGenerator;
use holonet\hdev\generator\table\TableMigrationGenerator;
use holonet\hdev\generator\migration\MigrationFileGenerator;
use holonet\hdev\generator\restable\ResTableMigrationGenerator;

class HdevGenerator {
	/**
	 * @var class-string<BaseGenerator>[] GENERATORS
	 */
	public const GENERATORS = array(
		'class' => ClassFileGenerator::class,
		'migration' => MigrationFileGenerator::class,
		'table' => TableMigrationGenerator::class,
		'resolutiontable' => ResTableMigrationGenerator::class,
	);

	/**
	 * Available options:
	 *  - generate_headers: Generate php file level comments or not
	 *  - src_dir: Base directory to assume the root of the application to be at (above src, tests, usw...).
	 * @var array $config Array with config options for the generator
	 */
	private array $config;

	public function __construct(array $config = array()) {
		$config['generate_headers'] = (bool)($config['generate_headers'] ?? true);
		if (!isset($config['src_dir'])) {
			throw new BadEnvironmentException('Cannot use hdev code generator without supplying "src_dir" option');
		}

		if (!is_dir($config['src_dir'])) {
			throw new BadEnvironmentException("Given src_dir '{$config['src_dir']}' is not a valid directory");
		}

		if (!isset($config['app_name'])) {
			throw new BadEnvironmentException('Cannot use hdev code generator without supplying "app_name" option');
		}

		$this->config = $config;
	}

	public function getGenerator(string $name): BaseGenerator {
		if (!isset(static::GENERATORS[$name])) {
			throw new InvalidArgumentException("Unknown generator '{$name}'");
		}

		/** @var class-string<BaseGenerator> $class */
		$class = static::GENERATORS[$name];

		return new $class();
	}

	public function getGlobalData(): array {
		$ret = array();
		if (isset($this->config['vendor_name'])) {
			$ret['baseNamespace'] = $this->config['vendor_name'];
		}
		if (isset($this->config['app_name'])) {
			$ret['appname'] = $this->config['app_name'];
		}

		return $ret;
	}

	public function run(BaseGenerator $generator, GeneratorInput $generatorInput): string {
		$file = $generator->run($generatorInput);

		$this->addHeader($file);

		$filepath = "{$this->config['src_dir']}/{$generatorInput->getFilePath()}";
		if (!is_dir(dirname($filepath))) {
			mkdir(dirname($filepath), 0755, true);
		}

		if (file_exists($filepath)) {
			throw new RuntimeException("Error: File '{$filepath}' does already exist.");
		}

		file_put_contents($filepath, $this->fixUglyCodePractices((string)$file));

		return $filepath;
	}

	private function addHeader(PhpFile $file): void {
		//in any case, add a comment to specify the file is auto-generated
		$file->addComment('@TODO This file is auto-generated.');

		if ($this->config['generate_headers']) {
			$top = '';

			if (isset($this->config['part_of'])) {
				$top .= "This file is part of {$this->config['part_of']}.\n";
			}

			if (isset($this->config['author_name'])) {
				$top .= "(c) {$this->config['author_name']}";
			}
			if (!empty($top)) {
				$file->addComment("{$top}\n");
			}
		}

		if ($this->config['generate_headers']) {
			$bottom = '';
			if (isset($this->config['license'])) {
				$bottom .= "@license {$this->config['license']}\n";
			}

			if (isset($this->config['author_name'])) {
				$bottom .= "@author {$this->config['author_name']} ";
				if (isset($this->config['author_email'])) {
					$bottom .= "<{$this->config['author_email']}>";
				}
			}
			if (!empty($bottom)) {
				$file->addComment("{$bottom}");
			}
		}
	}

	private function fixUglyCodePractices(string $code): string {
		$code = str_replace(array(
			"<?php\n\n",
		), array(
			"<?php\n",
		), $code);

		return preg_replace('/[\s]*\n[ \t]*{/m', ' {', $code);
	}
}
