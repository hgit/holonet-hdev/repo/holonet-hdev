<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\classfile;

use InvalidArgumentException;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use holonet\hdev\generator\BaseGenerator;
use holonet\hdev\generator\input\GeneratorInput;
use holonet\hdev\generator\classfile\input\ClassGeneratorInput;

class ClassFileGenerator extends BaseGenerator {
	public function getInputDef(): GeneratorInput {
		return new ClassGeneratorInput();
	}

	public function run(GeneratorInput $generatorInput): PhpFile {
		if (!$generatorInput instanceof ClassGeneratorInput) {
			throw new InvalidArgumentException(sprintf('Generator %s expects generator input of type %s', static::class, ClassGeneratorInput::class));
		}

		$file = parent::run($generatorInput);
		$this->generateClass($generatorInput, $this->generateNamespace($generatorInput, $file));

		return $file;
	}

	protected function generateClass(ClassGeneratorInput $generatorInput, PhpNamespace $namespace): ClassType {
		$class = $namespace->addClass($generatorInput->getClassname());
		if (($desc = $generatorInput->getDescription()) !== null) {
			$class->addComment($desc);
		}

		return $class;
	}

	protected function generateNamespace(ClassGeneratorInput $generatorInput, PhpFile $file): PhpNamespace {
		return $file->addNamespace($generatorInput->getNamespace());
	}
}
