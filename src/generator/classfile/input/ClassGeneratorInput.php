<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\classfile\input;

use holonet\hdev\generator\input\GeneratorInput;

class ClassGeneratorInput extends GeneratorInput {
	public string $baseNamespace;

	public string $classname;

	public ?string $description;

	public ?string $namespace;

	public function getAppNamespace(): string {
		return trim("{$this->baseNamespace}\\{$this->appname}", '\\');
	}

	public function getClassname(): string {
		return $this->classname;
	}

	public function getDescription(): ?string {
		return $this->description;
	}

	public function getFilename(): string {
		return "{$this->classname}.php";
	}

	public function getFilePath(): string {
		return str_replace('\\', '/', $this->namespace ?? '')."/{$this->getFilename()}";
	}

	public function getNamespace() {
		return trim("{$this->getAppNamespace()}\\{$this->namespace}", '\\');
	}
}
