<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator;

use Nette\PhpGenerator\PhpFile;
use holonet\hdev\generator\input\GeneratorInput;

abstract class BaseGenerator {
	abstract public function getInputDef(): GeneratorInput;

	public function run(GeneratorInput $generatorInput): PhpFile {
		return new PhpFile();
	}
}
