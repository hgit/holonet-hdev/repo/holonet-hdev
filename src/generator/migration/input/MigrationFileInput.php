<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\migration\input;

use holonet\hdev\generator\classfile\input\ClassGeneratorInput;

class MigrationFileInput extends ClassGeneratorInput {
	public const PROMPTS = array(
		'table' => 'name of the migration target table',
	);

	public string $classname = '_generated_';

	public ?string $description = '_generated_';

	public ?string $namespace = 'db\\migrate';

	public ?MigrationSentenceInput $sentence;

	/**
	 * Table we are changing with the generated migration.
	 */
	public string $table;

	public function getClassname(): string {
		return str_replace(' ', '', ucfirst($this->getTablename()).ucwords($this->sentence->sentence).'Migration');
	}

	public function getDescription(): string {
		return $this->sentence->sentence ?? '';
	}

	public function getFilename(): string {
		return time()."_{$this->getTablename()}_{$this->sentence->getFilePath()}";
	}

	public function getTablename(): string {
		return $this->table;
	}
}
