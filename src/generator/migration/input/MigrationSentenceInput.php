<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\migration\input;

use holonet\hdev\generator\input\GeneratorInput;
use holonet\hdev\generator\input\ExpectDecorator;

/**
 * @psalm-suppress MissingConstructor
 */
class MigrationSentenceInput extends GeneratorInput {
	public const PROMPTS = array(
		'sentence' => 'short sentence describing the purpose of the migration',
	);

	/**
	 * The sentence describing the migration.
	 */
	public string $sentence;

	public function getFilePath(): string {
		return mb_strtolower(implode('_', explode(' ', $this->sentence))).'.php';
	}

	public function getSchema(): ExpectDecorator {
		$ret = parent::getSchema();
		$sentence = $ret->get('sentence');
		/** @psalm-suppress UndefinedInterfaceMethod */
		$sentence->before(static fn (string $v) => implode(' ', explode('_', $v)));

		return $ret;
	}
}
