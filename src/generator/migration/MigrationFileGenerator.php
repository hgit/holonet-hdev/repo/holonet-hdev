<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\migration;

use LogicException;
use InvalidArgumentException;
use Nette\PhpGenerator\PhpFile;
use holonet\dbmigrate\Migration;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use holonet\dbmigrate\builder\TableBuilder;
use holonet\hdev\generator\input\GeneratorInput;
use holonet\hdev\generator\classfile\ClassFileGenerator;
use holonet\hdev\generator\migration\input\MigrationFileInput;
use holonet\hdev\generator\classfile\input\ClassGeneratorInput;

class MigrationFileGenerator extends ClassFileGenerator {
	public function getInputDef(): GeneratorInput {
		return new MigrationFileInput();
	}

	protected function addMigrationMethods(MigrationFileInput $generatorInput, ClassType $class): void {
		$changeTable = <<<CHANGETABLE
		\$this->schema->changeTable('{$generatorInput->getTablename()}', static function (TableBuilder \$t): void {
		    throw new LogicException('Not implemented yet');
		});
		CHANGETABLE;

		$method = $class->addMethod('up');
		$method->setReturnType('void');
		$method->setBody($changeTable);

		$method = $class->addMethod('down');
		$method->setReturnType('void');
		$method->setBody($changeTable);
	}

	protected function generateClass(ClassGeneratorInput $generatorInput, PhpNamespace $namespace): ClassType {
		if (!$generatorInput instanceof MigrationFileInput) {
			throw new InvalidArgumentException(sprintf('Generator %s expects generator input of type %s', static::class, MigrationFileInput::class));
		}

		$class = parent::generateClass($generatorInput, $namespace);
		$class->addExtend(Migration::class);
		$this->addMigrationMethods($generatorInput, $class);

		return $class;
	}

	protected function generateNamespace(ClassGeneratorInput $generatorInput, PhpFile $file): PhpNamespace {
		$namespace = parent::generateNamespace($generatorInput, $file);
		$namespace->addUse(Migration::class);
		$namespace->addUse(TableBuilder::class);
		$namespace->addUse(LogicException::class);

		return $namespace;
	}
}
