<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\table;

use Nette\PhpGenerator\ClassType;
use holonet\hdev\generator\input\GeneratorInput;
use holonet\hdev\generator\table\input\TableMigrationInput;
use holonet\hdev\generator\migration\MigrationFileGenerator;
use holonet\hdev\generator\migration\input\MigrationFileInput;

class TableMigrationGenerator extends MigrationFileGenerator {
	public function getInputDef(): GeneratorInput {
		return new TableMigrationInput();
	}

	protected function addMigrationMethods(MigrationFileInput $generatorInput, ClassType $class): void {
		$changeTable = <<<CREATETABLE
		\$this->schema->createTable('{$generatorInput->getTablename()}', static function (TableBuilder \$t): void {
		    throw new LogicException('Not implemented yet');
		});
		CREATETABLE;

		$method = $class->addMethod('up');
		$method->setReturnType('void');
		$method->setBody($changeTable);

		$method = $class->addMethod('down');
		$method->setReturnType('void');
		$method->setBody("\$this->schema->dropTable('{$generatorInput->getTablename()}');");
	}
}
