<?php
/**
 * This file is part of the holonet development tools package
 * (c) Matthias Lantsch.
 *
 * @license http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author  Matthias Lantsch <matthias.lantsch@bluewin.ch>
 */

namespace holonet\hdev\generator\table\input;

use holonet\hdev\generator\migration\input\MigrationFileInput;
use holonet\hdev\generator\migration\input\MigrationSentenceInput;

class TableMigrationInput extends MigrationFileInput {
	public const PROMPTS = array(
		'table' => 'name of the table to create',
	);

	public ?MigrationSentenceInput $sentence = null;

	public function getClassname(): string {
		return str_replace(' ', '', ucfirst($this->getTablename()).'CreateMigration');
	}

	public function getDescription(): string {
		return "create the '{$this->getTablename()}' table";
	}

	public function getFilename(): string {
		return time()."_{$this->getTablename()}_create.php";
	}
}
